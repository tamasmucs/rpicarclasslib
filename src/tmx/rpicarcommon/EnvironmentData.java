/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tmx.rpicarcommon;

import java.io.Serializable;

/**
 *
 * @author tamas
 */
public class EnvironmentData implements Serializable{

    public double getTemperature() {
        return temperature;
    }

    public double getDistance() {
        return distance;
    }

    private double temperature;
    private double distance;
    
    public EnvironmentData() {
    }

    public EnvironmentData(double temperature, double distance) {
        this.temperature = temperature;
        this.distance = distance;
    }
    
    @Override
    public String toString(){
        return this.temperature+"C | "+this.distance+" cm";
    }
}
