/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tmx.rpicarcommon;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;


public abstract class CarComponent implements Runnable {
    private InetAddress rpiAddress;
    protected ObjectOutputStream out;
    protected ObjectInputStream in;
    protected int componentPort;
    protected boolean asServer;
    private Socket socket;
    public CarComponent(String rpiAddress, int componentPort, boolean asServer) {
        try {
            this.rpiAddress = InetAddress.getByName(rpiAddress);
        } catch (UnknownHostException ex) {
            Logger.getLogger(CarComponent.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.componentPort = componentPort;
        this.asServer = asServer;
    }
    
    public CarComponent(int componentPort, boolean asServer) {
        this.componentPort = componentPort;
        this.asServer = asServer;
    }
    
    private void connectAsClient() {
        try {
            //Connect to RPi for sending movement commands
            socket = new Socket(rpiAddress, componentPort);
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    private void waitForClient() {
        try {
            ServerSocket sSocket = new ServerSocket(componentPort);
            Socket cSocket;
            cSocket = sSocket.accept();
            sSocket.close();
            in = new ObjectInputStream(cSocket.getInputStream());
            out = new ObjectOutputStream(cSocket.getOutputStream());
        } catch (IOException ex) {
            Logger.getLogger(CarComponent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void connect() {
        System.out.println(getClass().getSimpleName()+ " connecting. asServer= "+this.asServer);
        if(this.asServer){
            waitForClient();
        }else{
            connectAsClient();
        }
        System.out.println(getClass().getSimpleName()+" connected");
    }
    
    @Override
    public void run(){
        throw new UnsupportedOperationException();
    }
}
