/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tmx.rpicarcommon;

import java.io.Serializable;
import java.util.Arrays;


public class Direction implements Serializable{

    private int v = 0;
    private int h = 0;
    
    public synchronized int getV() {
        return v;
    }

    public synchronized int getH() {
        return h;
    }

    public Direction() {
        this.v = 0;
        this.h = 0;
    }

    public Direction(int v, int h) {
        this.v = v;
        this.h = h;
    }
    
    public synchronized boolean  isNull() {
        return (this.h == 0 && this.v == 0);
    }
    
    public synchronized void reset(){
        this.h=0;
        this.v=0;
    }
    
    @Override
    public String toString(){
        char[] result = {' ',' '};
        switch(this.v){
            case -1:
                result[0]='V';
                break;
            case 0:
                result[0]=' ';
                break;
            case 1:
                result[0]='^';
                break;
            default:
                result[0]='e'; //error
                break;
        }
        
        switch(this.h){
            case -1:
                result[1]='<';
                break;
            case 0:
                result[1]=' ';
                break;
            case 1:
                result[1]='>';
                break;
            default:
                result[1]='e'; //error
                break;
        }
        return Arrays.toString(result);
    }
}
